<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Messages'] = [
    'All'           => 'All',
    'AverageAmount' => 'Avg. Amount',
    'BCC'           => 'BCC',
    'CC'            => 'CC',
    'Date'          => 'Date',
    'Draft'         => 'Draft',
    'From'          => 'From',
    'Inbox'         => 'Inbox',
    'Interval'      => 'Interval',
    'Mailboxes'     => 'Mailboxes',
    'Messages'      => 'Messages',
    'Month'         => 'Month',
    'New'           => 'New',
    'Outbox'        => 'Outbox',
    'Received'      => 'Received',
    'Sent'          => 'Sent',
    'Spam'          => 'Spam',
    'Statistics'    => 'Statistics',
    'Status'        => 'Status',
    'Subject'       => 'Subject',
    'Tag'           => 'Tag',
    'To'            => 'To',
    'Today'         => 'Today',
    'Trash'         => 'Trash',
    'Week'          => 'Week',
    'Year'          => 'Year',
];
