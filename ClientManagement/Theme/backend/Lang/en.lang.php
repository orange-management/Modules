<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['ClientManagement'] = [
    'Accounting'   => 'Accounting',
    'Address'      => 'Address',
    'Addresses'    => 'Addresses',
    'AreaManager'  => 'Area Manager',
    'Articlegroup' => 'Articlegroup',
    'Bonus'        => 'Bonus',
    'Business'     => 'Business',
    'City'         => 'City',
    'Client'       => 'Client',
    'Clients'      => 'Clients',
    'Contact'      => 'Contact',
    'Country'      => 'Country',
    'Creditcard'   => 'Creditcard',
    'Default'      => 'Default',
    'Delivery'     => 'Delivery',
    'Discount'     => 'Discount',
    'DiscountP'    => 'Discount %',
    'Email'        => 'Email',
    'Fax'          => 'Fax',
    'Files'        => 'Files',
    'Freightage'   => 'Freightage',
    'Group'        => 'Group',
    'Info'         => 'Info',
    'Invoice'      => 'Invoice',
    'IsDefault'    => 'Is default?',
    'Master'       => 'Master',
    'Name'         => 'Name',
    'Name1'        => 'Name1',
    'Name2'        => 'Name2',
    'Name3'        => 'Name3',
    'Office'       => 'Office',
    'Payment'      => 'Payment',
    'PaymentTerm'  => 'Payment Term',
    'Phone'        => 'Phone',
    'Prices'       => 'Prices',
    'Price'        => 'Price',
    'Private'      => 'Private',
    'Productgroup' => 'Productgroup',
    'Purchase'     => 'Purchase',
    'Quantity'     => 'Quantity',
    'Sales'        => 'Sales',
    'Segment'      => 'Segment',
    'Subtype'      => 'Subtype',
    'Support'      => 'Support',
    'Type'         => 'Type',
    'Wire'         => 'Wire',
    'Zip'          => 'Zip',
];
