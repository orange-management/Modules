<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
/**
 * @var \phpOMS\Views\View $this
 */

echo $this->getData('nav')->render(); ?>

<section class="box w-25 floatLeft">
    <h1>Line Chart</h1>
    <div class="inner">
        <a href="<?= \phpOMS\Uri\UriFactory::build('/{/lang}/backend/chart/create/line'); ?>" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/thumb-line-chart.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Area Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Bar Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Column Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Pie Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Point Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Pyramid Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Radar Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Bubble Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>High Low Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Candlestick Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Gantt Chart</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>

<section class="box w-25 floatLeft">
    <h1>Map</h1>
    <div class="inner">
        <a href="#" class="wf-100 centerText" style="background: #fff; display: inline-block">
            <img src="/Modules/Chart/Img/chart-thumb.png">
        </a>
    </div>
</section>
