<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Modules\Chart;

use Modules\Navigation\Models\Navigation;
use Modules\Navigation\Views\NavigationView;
use phpOMS\Asset\AssetType;
use phpOMS\Contract\RenderableInterface;
use phpOMS\Message\RequestAbstract;
use phpOMS\Message\RequestDestination;
use phpOMS\Message\ResponseAbstract;
use phpOMS\Module\ModuleAbstract;
use phpOMS\Module\WebInterface;
use phpOMS\Views\View;
use phpOMS\Views\ViewLayout;

/**
 * Calendar controller class.
 *
 * @category   Modules
 * @package    Chart
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Controller extends ModuleAbstract implements WebInterface
{

    /**
     * Module path.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_PATH = __DIR__;

    /**
     * Module version.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_VERSION = '1.0.0';

    /**
     * Module name.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_NAME = 'Chart';

    /**
     * Localization files.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $localization = [
        RequestDestination::BACKEND => [''],
    ];

    /**
     * Providing.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $providing = [];

    /**
     * Dependencies.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $dependencies = [
    ];

    /**
     * Routing elements.
     *
     * @var array
     * @since 1.0.0
     */
    protected static $routes = [
        '^.*/backend/chart/create$'        => [['dest' => '\Modules\Chart\Controller:viewChartCreate', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/chart/create/line.*$' => [
            ['dest' => '\Modules\Chart\Controller:setUpChartEditor', 'method' => 'GET', 'type' => ViewLayout::NULL],
            ['dest' => '\Modules\Chart\Controller:viewChartCreateLine', 'method' => 'GET', 'type' => ViewLayout::MAIN],
        ],
        '^.*/backend/chart/list.*$'        => [['dest' => '\Modules\Chart\Controller:viewChartList', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
    ];

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setUpChartEditor(RequestAbstract $request, ResponseAbstract $response, $data = null)
    {
        $head = $response->getHead();
        $head->addAsset(AssetType::CSS, $request->getUri()->getBase() . 'cssOMS/chart/chart.css');
        $head->addAsset(AssetType::CSS, $request->getUri()->getBase() . 'cssOMS/chart/chart_line.css');
        $head->addAsset(AssetType::JS, 'http://d3js.org/d3.v3.min.js');
        $head->addAsset(AssetType::JS, $request->getUri()->getBase() . 'Modules/Chart/ModuleChart.js');
        $head->addAsset(AssetType::JS, $request->getUri()->getBase() . 'jsOMS/Chart/Chart.js');
        $head->addAsset(AssetType::JSLATE, $request->getUri()->getBase() . 'jsOMS/Chart/LineChart.js');
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewChartCreate(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Chart/Theme/Backend/chart-create');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1004101001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewChartCreateLine(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Chart/Theme/Backend/chart-create-line');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1004101001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewChartList(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Chart/Theme/Backend/chart-list');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1004101001, $request, $response));

        return $view;
    }

}
