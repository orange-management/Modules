<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['News'] = [
    'Additional'  => 'Additional',
    'Archive'     => 'Archive',
    'Author'      => 'Author',
    'Date'        => 'Date',
    'Draft'       => 'Draft',
    'Featured'    => 'Featured',
    'Groups'      => 'Groups',
    'Headline'    => 'Headline',
    'Headlines'   => 'Headlines',
    'Link'        => 'Link',
    'News'        => 'News',
    'Permissions' => 'Permissions',
    'Plain'       => 'Plain',
    'Preview'     => 'Preview',
    'Publish'     => 'Publish',
    'Settings'    => 'Settings',
    'Status'      => 'Status',
    'Title'       => 'Title',
    'Type'        => 'Type',
    'Visible'     => 'Visible',
    'TYPE0'       => 'Article',
    'TYPE1'       => 'Link',
    'TYPE2'       => 'Headline',
];
