<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Editor'] = [
    'Created'    => 'Created',
    'Creator'    => 'Creator',
    'Documents'  => 'Documents',
    'Editor'     => 'Editor',
    'GroupUser'  => 'Group/User',
    'Insert'     => 'Insert',
    'Layout'     => 'Layout',
    'Name'       => 'Name',
    'Permission' => 'Permission',
    'Preview'    => 'Preview',
    'Start'      => 'Start',
    'Text'       => 'Text',
];
