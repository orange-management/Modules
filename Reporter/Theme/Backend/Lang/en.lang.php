<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Reporter'] = [
    'Add'              => 'Add',
    'CentralizedDB'    => 'Centralized DB',
    'CentralizedFiles' => 'Centralized Files',
    'Created'          => 'Created',
    'Creator'          => 'Creator',
    'CreatedBy'        => 'Created By',
    'Data'             => 'Data',
    'Database'         => 'Database',
    'Dataset'          => 'Dataset',
    'Datasets'         => 'Datasets',
    'Description'      => 'Description',
    'Expected'         => 'Expected',
    'Export'           => 'Export',
    'Edit'             => 'Edit',
    'Files'            => 'Files',
    'FileNames'        => 'File Names',
    'IndividualDB'     => 'Individual DB',
    'Info'             => 'Info',
    'Language'         => 'Language',
    'Media'            => 'Media',
    'MediaDirectory'   => 'Media Directory',
    'Modified'         => 'Modified',
    'Name'             => 'Name',
    'New'              => 'New',
    'Other'            => 'Other',
    'Overview'         => 'Overview',
    'Permission'       => 'Permission',
    'Reporter'         => 'Reporter',
    'Reports'          => 'Reports',
    'Report'           => 'Report',
    'Select'           => 'Select',
    'Storage'          => 'Storage',
    'Source'           => 'Source',
    'Sources'          => 'Sources',
    'Template'         => 'Template',
    'Title'            => 'Title',
    'Type'             => 'Type',
    'Updated'          => 'Updated',
];
