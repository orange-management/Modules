<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Marketing'] = [
    'Budget'      => 'Budget',
    'Description' => 'Description',
    'Expenses'    => 'Expenses',
    'End'         => 'End',
    'Event'       => 'Event',
    'Events'      => 'Events',
    'Limit'       => 'Limit',
    'Location'    => 'Location',
    'Promotion'   => 'Promotion',
    'Sales'       => 'Sales',
    'Start'       => 'Start',
    'Status'      => 'Status',
    'Title'       => 'Title',
    'Type'        => 'Type',
];
