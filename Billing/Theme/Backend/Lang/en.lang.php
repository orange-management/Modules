<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Billing'] = [
    'Address'      => 'Address',
    'Addresses'    => 'Addresses',
    'Bonus'        => 'Bonus',
    'City'         => 'City',
    'Client'       => 'Client',
    'ClientID'     => 'Client ID',
    'Confirmation' => 'Confirmation',
    'Country'      => 'Country',
    'Created'      => 'Created',
    'CreditNote'   => 'Credit Note',
    'Delivery'     => 'Delivery',
    'DeliveryNote' => 'Delivery Note',
    'Discount'     => 'Discount',
    'DiscountP'    => 'Discount %',
    'Due'          => 'Due',
    'Freightage'   => 'Freightage',
    'Gross'        => 'Gross',
    'Invoice'      => 'Invoice',
    'Invoices'     => 'Invoices',
    'Item'         => 'Item',
    'Items'        => 'Items',
    'Name'         => 'Name',
    'Net'          => 'Net',
    'Offer'        => 'Offer',
    'Price'        => 'Price',
    'Quantity'     => 'Quantity',
    'Recipient'    => 'Recipient',
    'Shipment'     => 'Shipment',
    'Source'       => 'Source',
    'Supplier'     => 'Supplier',
    'SupplierID'   => 'Supplier ID',
    'Tax'          => 'Tax',
    'Total'        => 'Total',
    'Type'         => 'Type',
    'Variation'    => 'Variation',
    'Zip'          => 'Zip',
];
