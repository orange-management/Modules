## Installation

The order of link elements inside the `Navigation.install.json` file needs to be unique. In case the order is not unique it's possible that the link order can vary from page to page.
