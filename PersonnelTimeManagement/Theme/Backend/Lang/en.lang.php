<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['PersonnelTimeManagement'] = [
    'All'            => 'All',
    'Date'           => 'Date',
    'Day'            => 'Day',
    'days'           => 'days',
    'Duration'       => 'Duration',
    'Editable'       => 'Editable',
    'End'            => 'End',
    'General'        => 'General',
    'hours'          => 'hours',
    'Interval'       => 'Interval',
    'Late'           => 'Late',
    'Month'          => 'Month',
    'Name'           => 'Name',
    'New'            => 'New',
    'Off'            => 'Off',
    'Other'          => 'Other',
    'Planning'       => 'Planning',
    'Remote'         => 'Remote',
    'Settings'       => 'Settings',
    'Sick'           => 'Sick',
    'Start'          => 'Start',
    'Status'         => 'Status',
    'Surplus'        => 'Surplus',
    'TimeManagement' => 'Time Management',
    'Travel'         => 'Travel',
    'Type'           => 'Type',
    'Vacation'       => 'Vacation',
    'Week'           => 'Week',
    'Work'           => 'Work',
    'Working'        => 'Working',
    'Year'           => 'Year',
];
