<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Surveys'] = [
    'Additional'     => 'Additional',
    'Answer'         => 'Answer',
    'Checkbox'       => 'Checkbox',
    'CoreData'       => 'Core Data',
    'Created'        => 'Created',
    'Creator'        => 'Creator',
    'Date'           => 'Date',
    'Description'    => 'Description',
    'Dropdown'       => 'Dropdown',
    'End'            => 'End',
    'Group'          => 'Group',
    'Question'       => 'Question',
    'Questions'      => 'Questions',
    'Manager'        => 'Manager',
    'Name'           => 'Name',
    'Number'         => 'Number',
    'Questionee'     => 'Questionee',
    'Radio'          => 'Radio',
    'Receiver'       => 'Receiver',
    'Reference'      => 'Reference',
    'Responsibility' => 'Responsibility',
    'Result'         => 'Result',
    'ResultPublic'   => 'Result public?',
    'Section'        => 'Section',
    'Start'          => 'Start',
    'Status'         => 'Status',
    'Survey'         => 'Survey',
    'Surveys'        => 'Surveys',
    'Text'           => 'Text',
    'Title'          => 'Title',
    'Type'           => 'Type',
    'User'           => 'User',
    'UserGroup'      => 'User/Group',
];
