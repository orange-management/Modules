<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Modules\AccountsPayable;

use Modules\Navigation\Models\Navigation;
use Modules\Navigation\Views\NavigationView;
use phpOMS\Contract\RenderableInterface;
use phpOMS\Message\RequestAbstract;
use phpOMS\Message\RequestDestination;
use phpOMS\Message\ResponseAbstract;
use phpOMS\Module\ModuleAbstract;
use phpOMS\Module\WebInterface;
use phpOMS\Views\View;
use phpOMS\Views\ViewLayout;

/**
 * Sales class.
 *
 * @category   Modules
 * @package    Modules\Accountsreceivable
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Controller extends ModuleAbstract implements WebInterface
{

    /**
     * Module path.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_PATH = __DIR__;

    /**
     * Module version.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_VERSION = '1.0.0';

    /**
     * Module name.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_NAME = 'AccountsPayable';

    /**
     * Localization files.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $localization = [
        RequestDestination::BACKEND => [''],
    ];

    /**
     * Routing elements.
     *
     * @var array
     * @since 1.0.0
     */
    protected static $routes = [
        '^.*/backend/accounting/payable/list.*$'         => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorList', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/create.*$'       => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorCreate', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/profile.*$'      => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorProfile', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/outstanding.*$'  => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorOutstanding', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/age.*$'          => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorAge', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/payable.*$'      => [['dest' => '\Modules\AccountsPayable\Controller:viewCreditorPayable', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/journal/list.*$' => [['dest' => '\Modules\AccountsPayable\Controller:viewJournalList', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/entries.*$' => [['dest' => '\Modules\AccountsPayable\Controller:viewEntriesList', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/accounting/payable/analyze.*$' => [['dest' => '\Modules\AccountsPayable\Controller:viewAnalyzeDashboard', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
    ];

    /**
     * Providing.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $providing = [];

    /**
     * Dependencies.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $dependencies = [
        'Media',
    ];

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewCreditorList(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/AccountsPayable/Theme/Backend/creditor-list');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1005001001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewCreditorCreate(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/AccountsPayable/Theme/Backend/creditor-create');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1005001001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewCreditorProfile(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/AccountsPayable/Theme/Backend/creditor-profile');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1005001001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewEntriesList(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/AccountsPayable/Theme/Backend/entries-list');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1005001001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewAnalyzeDashboard(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/AccountsPayable/Theme/Backend/analyze-dashboard');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1005001001, $request, $response));

        return $view;
    }

}
