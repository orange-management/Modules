<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Navigation'] = [
    'Categories'     => 'Categories',
    'Causes'         => 'Causes',
    'Cockpit'        => 'Cockpit',
    'Create'         => 'Create',
    'Delete'         => 'Delete',
    'Departments'    => 'Departments',
    'Projects'       => 'Projects',
    'Processes'      => 'Processes',
    'Risks'          => 'Risks',
    'RiskManagement' => 'Risk Management',
    'Settings'       => 'Settings',
    'Solutions'      => 'Solutions',
    'Units'          => 'Units',
];
