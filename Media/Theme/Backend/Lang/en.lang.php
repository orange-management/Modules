<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Media'] = [
    'Account'     => 'Account',
    'Author'      => 'Author',
    'Changed'     => 'Changed',
    'Changedby'   => 'Changed by',
    'Created'     => 'Created',
    'Creator'     => 'Creator',
    'Data'        => 'Data',
    'Editability' => 'Editability',
    'Extension'   => 'Extension',
    'Files'       => 'Files',
    'Media'       => 'Media',
    'Name'        => 'Name',
    'Permission'  => 'Permission',
    'Preview'     => 'Preview',
    'Settings'    => 'Settings',
    'Size'        => 'Size',
    'Type'        => 'Type',
    'Upload'      => 'Upload',
    'Visibility'  => 'Visibility',
];
