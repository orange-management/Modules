<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Support'] = [
    'Account'            => 'Account',
    'All'                => 'All',
    'AverageProcessTime' => 'Avg. Process Time',
    'AverageAmount'      => 'Average Amount',
    'Created'            => 'Created',
    'Creator'            => 'Creator',
    'Day'                => 'Day',
    'Department'         => 'Department',
    'Description'        => 'Description',
    'Due'                => 'Due',
    'Files'              => 'Files',
    'Forwarded'          => 'Forwarded',
    'From'               => 'From',
    'Group'              => 'Group',
    'Interval'           => 'Interval',
    'InTime'             => 'In Time',
    'Message'            => 'Message',
    'Media'              => 'Media',
    'Month'              => 'Month',
    'Name'               => 'Name',
    'New'                => 'New',
    'Open'               => 'Open',
    'Person'             => 'Person',
    'Priority'           => 'Priority',
    'Received'           => 'Received',
    'Receiver'           => 'Receiver',
    'Redirected'         => 'Redirected',
    'Responsible'        => 'Responsible',
    'Select'             => 'Select',
    'Settings'           => 'Settings',
    'Size'               => 'Size',
    'Statistics'         => 'Statistics',
    'Status'             => 'Status',
    'Support'            => 'Support',
    'Ticket'             => 'Ticket',
    'Tickets'            => 'Tickets',
    'Title'              => 'Title',
    'To'                 => 'To',
    'Today'              => 'Today',
    'Topic'              => 'Topic',
    'Type'               => 'Type',
    'Week'               => 'Week',
    'Year'               => 'Year',
];
