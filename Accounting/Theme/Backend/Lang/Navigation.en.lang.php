<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Navigation'] = [
    'Accounting'      => 'Accounting',
    'AccountPostings' => 'Account Postings',
    'Balance'         => 'Balance',
    'BatchPostings'   => 'Batch Postings',
    'CostCenters'     => 'Cost Centers',
    'CostObjects'     => 'Cost Objects',
    'Creditors'       => 'Creditors',
    'Debitors'        => 'Debitors',
    'Entries'         => 'Entries',
    'Postings'        => 'Postings',
    'Predefined'      => 'Predefined',
];
