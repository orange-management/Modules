<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Accounting'] = [
    'Account'         => 'Account',
    'Accounts'        => 'Accounts',
    'BatchPostings'   => 'Batch Postings',
    'Charts'          => 'Charts',
    'ContraAccount'   => 'Contra Account',
    'CostCenter'      => 'Cost Center',
    'CostCenters'     => 'Cost Centers',
    'CostObject'      => 'Cost Object',
    'CostObjects'     => 'Cost Objects',
    'Created'         => 'Created',
    'Creator'         => 'Creator',
    'Credit'          => 'Credit',
    'Debit'           => 'Debit',
    'Due'             => 'Due',
    'Entries'         => 'Entries',
    'EntryDate'       => 'Entry Date',
    'Evaluation'      => 'Evaluation',
    'ExternalVoucher' => 'External Voucher',
    'Incoming'        => 'Incoming',
    'GL'              => 'GL',
    'List'            => 'List',
    'Name'            => 'Name',
    'Outgoing'        => 'Outgoing',
    'Parent'          => 'Parent',
    'Receipt'         => 'Receipt',
    'ReceiptDate'     => 'Receipt Date',
    'Stack'           => 'Stack',
    'TAccount'        => 'T-Account',
    'Text'            => 'Text',
    'To'              => 'To',
    'Total'           => 'Total',
    'Type'            => 'Type',
];
