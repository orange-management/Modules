<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['AccountsReceivable'] = [
    'AccountsReceivable' => 'Accounts Receivable',
    'Address'            => 'Address',
    'AgePattern'         => 'Age Pattern',
    'Balance'            => 'Balance',
    'CreditLimit'        => 'Credit Limit',
    'City'               => 'City',
    'Country'            => 'Country',
    'Due'                => 'Due',
    'DeliveryStatus'     => 'Delivery Status',
    'DSO'                => 'DSO',
    'Name1'              => 'Name1',
    'Name2'              => 'Name2',
    'Name3'              => 'Name3',
    'Paid'               => 'Paid',
    'Selected'           => 'Selected',
    'Status'             => 'Status',
    'Zip'                => 'Zip',
];
