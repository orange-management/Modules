<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Tasks'] = [
    'Account'            => 'Account',
    'All'                => 'All',
    'AverageProcessTime' => 'Avg. Process Time',
    'AverageAmount'      => 'Average Amount',
    'BCC'                => 'BCC',
    'CC'                 => 'CC',
    'Created'            => 'Created',
    'Creator'            => 'Creator',
    'Default'            => 'Default',
    'Day'                => 'Day',
    'Due'                => 'Due',
    'Forwarded'          => 'Forwarded',
    'From'               => 'From',
    'Group'              => 'Group',
    'Interval'           => 'Interval',
    'InTime'             => 'In Time',
    'Message'            => 'Message',
    'Media'              => 'Media',
    'Month'              => 'Month',
    'Name'               => 'Name',
    'New'                => 'New',
    'Open'               => 'Open',
    'Person'             => 'Person',
    'Priority'           => 'Priority',
    'Received'           => 'Received',
    'Redirected'         => 'Redirected',
    'Select'             => 'Select',
    'Settings'           => 'Settings',
    'SharedVisibility'   => 'Visibility is shared across all',
    'Size'               => 'Size',
    'Statistics'         => 'Statistics',
    'Status'             => 'Status',
    'Task'               => 'Task',
    'Tasks'              => 'Tasks',
    'Template'           => 'Template',
    'Title'              => 'Title',
    'To'                 => 'To',
    'Today'              => 'Today',
    'Type'               => 'Type',
    'Upload'             => 'Upload',
    'Week'               => 'Week',
    'Year'               => 'Year',
    'S1'                 => 'Open',
    'S2'                 => 'Working',
    'S3'                 => 'Suspended',
    'S4'                 => 'Canceled',
    'S5'                 => 'Done',
];
