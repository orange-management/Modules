<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Modules\Tasks;

use Model\Message\Redirect;
use Modules\Tasks\Models\Task;
use Modules\Tasks\Models\TaskElement;
use Modules\Tasks\Models\TaskMapper;
use Modules\Tasks\Models\TaskStatus;
use Modules\Tasks\Models\TaskType;
use phpOMS\Contract\RenderableInterface;
use phpOMS\Message\RequestAbstract;
use phpOMS\Message\RequestDestination;
use phpOMS\Message\ResponseAbstract;
use phpOMS\Module\ModuleAbstract;
use phpOMS\Module\WebInterface;
use phpOMS\Uri\UriFactory;
use phpOMS\Views\View;
use phpOMS\Views\ViewLayout;

/**
 * Task class.
 *
 * @category   Modules
 * @package    Modules\Tasks
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Controller extends ModuleAbstract implements WebInterface
{

    /**
     * Module path.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_PATH = __DIR__;

    /**
     * Module version.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_VERSION = '1.0.0';

    /**
     * Module name.
     *
     * @var string
     * @since 1.0.0
     */
    const MODULE_NAME = 'Tasks';

    /**
     * Localization files.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $localization = [
        RequestDestination::BACKEND => [''],
    ];

    /**
     * Providing.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $providing = [];

    /**
     * Dependencies.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $dependencies = [
    ];

    /**
     * Routing elements.
     *
     * @var array
     * @since 1.0.0
     */
    protected static $routes = [
        '^.*/backend/task/dashboard.*$' => [['dest' => '\Modules\Tasks\Controller:viewTaskDashboard', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/task/single.*$'    => [['dest' => '\Modules\Tasks\Controller:viewTaskView', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/task/create.*$'    => [['dest' => '\Modules\Tasks\Controller:viewTaskCreate', 'method' => 'GET', 'type' => ViewLayout::MAIN],],
        '^.*/backend/task/analysis.*$'  => [['dest' => '\Modules\Tasks\Controller:viewTaskAnalysis', 'method' => 'GET', 'type' => ViewLayout::MAIN],],

        '^.*/api/task/create.*$' => [['dest' => '\Modules\Tasks\Controller:apiTaskCreate', 'method' => 'POST', 'type' => ViewLayout::NULL],],
    ];

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewTaskDashboard(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Tasks/Theme/Backend/task-dashboard');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1001101001, $request, $response));

        $taskMapper = new TaskMapper($this->app->dbPool->get());
        $tasks      = $taskMapper->getNewest(25);
        $view->addData('tasks', $tasks);

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewTaskView(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Tasks/Theme/Backend/task-single');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1001101001, $request, $response));

        $taskMapper = new TaskMapper($this->app->dbPool->get());
        $task       = $taskMapper->get((int) $request->getData('id'));
        $view->addData('task', $task);

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewTaskCreate(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Tasks/Theme/Backend/task-create');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1001101001, $request, $response));

        return $view;
    }

    /**
     * @param RequestAbstract  $request  Request
     * @param ResponseAbstract $response Response
     * @param mixed            $data     Generic data
     *
     * @return RenderableInterface
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function viewTaskAnalysis(RequestAbstract $request, ResponseAbstract $response, $data = null) : RenderableInterface
    {
        $view = new View($this->app, $request, $response);
        $view->setTemplate('/Modules/Tasks/Theme/Backend/task-analysis');
        $view->addData('nav', $this->app->moduleManager->get('Navigation')->createNavigationMid(1001101001, $request, $response));

        return $view;
    }

    public function apiTaskCreate(RequestAbstract $request, ResponseAbstract $response, $data = null)
    {
        $taskMapper = new TaskMapper($this->app->dbPool->get());

        $task = new Task();
        $task->setTitle($request->getData('title') ?? '');
        $task->setDescription($request->getData('description') ?? '');
        $task->setCreatedBy($request->getAccount());
        $task->setCreatedAt(new \DateTime('now'));
        $task->setDue(new \DateTime($request->getData('due') ?? 'now'));
        $task->setStatus(TaskStatus::OPEN);
        $task->setType(TaskType::TASK);

        $element = new TaskElement();
        $element->setForwarded($request->getData('forward') ?? $request->getAccount());
        $element->setCreatedAt($task->getCreatedAt());
        $element->setCreatedBy($task->getCreatedBy());
        $element->setDue($task->getDue());
        $element->setStatus(TaskStatus::OPEN);

        $task->addElement($element);

        $taskMapper->create($task);
        $response->set($request->__toString(), new Redirect(UriFactory::build('http://127.0.0.1/{/lang}/backend/task/single?id=' . $task->getId())));
    }

}
