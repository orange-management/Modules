<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Monitoring'] = [
    'Alerts'      => 'Alerts',
    'All'         => 'All',
    'Backtrace'   => 'Backtrace',
    'CPUUsage'    => 'CPU Usage',
    'Created'     => 'Created',
    'CreatedBy'   => 'Created By',
    'Criticals'   => 'Criticals',
    'Debug'       => 'Debug',
    'Description' => 'Description',
    'DiskUsage'   => 'DiskUsage',
    'Emergencies' => 'Emergencies',
    'Errors'      => 'Errors',
    'Exception'   => 'Exception',
    'File'        => 'File',
    'Info'        => 'Info',
    'Level'       => 'Level',
    'Line'        => 'Line',
    'Logs'        => 'Logs',
    'MemoryLimit' => 'Memory Limit',
    'Message'     => 'Message',
    'Notices'     => 'Notices',
    'Status'      => 'Status',
    'System'      => 'System',
    'Total'       => 'Total',
    'OS'          => 'OS',
    'Penetrators' => 'Penetrators',
    'RAMUsage'    => 'RAM Usage',
    'Release'     => 'Release',
    'Report'      => 'Report',
    'Source'      => 'Source',
    'SystemRAM'   => 'System RAM',
    'Theme'       => 'Theme',
    'Time'        => 'Time',
    'Timestamp'   => 'Timestamp',
    'Uri'         => 'Uri',
    'Version'     => 'Version',
    'Warnings'    => 'Warnings',
];
