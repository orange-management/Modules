<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['ProjectManagement'] = [
    'Active'         => 'Active',
    'Budget'         => 'Budget',
    'Canceled'       => 'Canceled',
    'Created'        => 'Created',
    'Creator'        => 'Creator',
    'Description'    => 'Description',
    'Due'            => 'Due',
    'Files'          => 'Files',
    'Finished'       => 'Finished',
    'Hold'           => 'Hold',
    'Inactive'       => 'Inactive',
    'Manager'        => 'Manager',
    'Name'           => 'Name',
    'Other'          => 'Other',
    'Project'        => 'Project',
    'Projects'       => 'Projects',
    'Responsibility' => 'Responsibility',
    'Start'          => 'Start',
    'Status'         => 'Status',
    'Title'          => 'Title',
    'UserGroup'      => 'User/Group',
];
