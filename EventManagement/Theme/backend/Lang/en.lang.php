<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['EventManagement'] = [
    'Account'         => 'Account',
    'Amount'          => 'Amount',
    'CoreData'        => 'Core Data',
    'Description'     => 'Description',
    'Elements'        => 'Elements',
    'End'             => 'End',
    'Event'           => 'Event',
    'Events'          => 'Events',
    'EventManagement' => 'Event Management',
    'Files'           => 'Files',
    'Group'           => 'Group',
    'Info'            => 'Info',
    'People'          => 'People',
    'Manager'         => 'Manager',
    'Name'            => 'Name',
    'Permissions'     => 'Permissions',
    'Responsibility'  => 'Responsibility',
    'Start'           => 'Start',
    'Status'          => 'Status',
    'Title'           => 'Title',
    'User'            => 'User',
    'UserGroup'       => 'User/Group',
];
