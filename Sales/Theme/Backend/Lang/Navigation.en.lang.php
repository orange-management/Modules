<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Navigation'] = [
    'Analysis' => 'Analysis',
    'Articles' => 'Articles',
    'Clients'  => 'Clients',
    'Create'   => 'Create',
    'Invoices' => 'Invoices',
    'Invoice'  => 'Invoice',
    'Profile'  => 'Profile',
    'Sales'    => 'Sales',
];
