<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['WarehouseManagement'] = [
    'All'         => 'All',
    'Amount'      => 'Amount',
    'Arrivals'    => 'Arrivals',
    'Articles'    => 'Articles',
    'Article'     => 'Article',
    'City'        => 'City',
    'Consignee'   => 'Consignee',
    'Consignor'   => 'Consignor',
    'Country'     => 'Country',
    'Date'        => 'Date',
    'Description' => 'Description',
    'Interval'    => 'Interval',
    'Location'    => 'Location',
    'Matchcode'   => 'Matchcode',
    'Month'       => 'Month',
    'Name'        => 'Name',
    'Order'       => 'Order',
    'Quantity'    => 'Quantity',
    'Reference'   => 'Reference',
    'Shipping'    => 'Shipping',
    'Statistics'  => 'Statistics',
    'Stock'       => 'Stock',
    'Street'      => 'Street',
    'Today'       => 'Today',
    'Type'        => 'Type',
    'Week'        => 'Week',
    'Year'        => 'Year',
    'Zip'         => 'Zip',
];
