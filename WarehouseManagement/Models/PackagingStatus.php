<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Modules\Warehousing\Models;

use phpOMS\Datatypes\Enum;

/**
 * Packaging status enum.
 *
 * @category   Warehousing
 * @package    Modules
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
abstract class PackagingStatus extends Enum
{
    const PENDING = 0;

    const PACKING = 1;

    const PACKED = 2;

    const SUSPENDED = 3;

    const CANCELED = 4;
}
