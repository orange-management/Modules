<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Profile'] = [
    'Activity'           => 'Activity',
    'Birthday'           => 'Birthday',
    'ContactInformation' => 'Contact Information',
    'Community'          => 'Community',
    'Email'              => 'Email',
    'LastLogin'          => 'Last Login',
    'Name'               => 'Name',
    'Occupation'         => 'Occupation',
    'OFF'                => 'OFF',
    'ON'                 => 'ON',
    'Phone'              => 'Phone',
    'Profile'            => 'Profile',
    'Profiles'           => 'Profiles',
    'Ranks'              => 'Ranks',
    'Registered'         => 'Registered',
    'Skype'              => 'Skype',
    'Status'             => 'Status',
];
