<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['Checklist'] = [
    'Checklist'   => 'Checklist',
    'Checklists'  => 'Checklists',
    'Created'     => 'Created',
    'Creator'     => 'Creator',
    'Description' => 'Description',
    'Files'       => 'Files',
    'General'     => 'General',
    'Name'        => 'Name',
    'Permissions' => 'Permissions',
    'Status'      => 'Status',
    'Tasks'       => 'Tasks',
    'Templates'   => 'Templates',
    'Title'       => 'Title',
];
