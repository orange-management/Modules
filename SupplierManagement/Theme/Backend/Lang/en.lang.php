<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$MODLANG['SupplierManagement'] = [
    'Accounting'   => 'Accounting',
    'Address'      => 'Address',
    'Addresses'    => 'Addresses',
    'Articlegroup' => 'Articlegroup',
    'City'         => 'City',
    'Contact'      => 'Contact',
    'Country'      => 'Country',
    'Creditcard'   => 'Creditcard',
    'Default'      => 'Default',
    'Delivery'     => 'Delivery',
    'Email'        => 'Email',
    'Fax'          => 'Fax',
    'Files'        => 'Files',
    'Freightage'   => 'Freightage',
    'Group'        => 'Group',
    'Info'         => 'Info',
    'Invoice'      => 'Invoice',
    'IsDefault'    => 'Is default',
    'Master'       => 'Master',
    'Name'         => 'Name',
    'Name1'        => 'Name1',
    'Name2'        => 'Name2',
    'Name3'        => 'Name3',
    'Office'       => 'Office',
    'PaymentTerm'  => 'Payment Term',
    'Payment'      => 'Payment',
    'Phone'        => 'Phone',
    'Productgroup' => 'Productgroup',
    'Purchase'     => 'Purchase',
    'Sales'        => 'Sales',
    'Segment'      => 'Segment',
    'Subtype'      => 'Subtype',
    'Support'      => 'Support',
    'Supplier'     => 'Supplier',
    'Suppliers'     => 'Suppliers',
    'Type'         => 'Type',
    'Wire'         => 'Wire',
    'Zip'          => 'Zip',
];
